package week4.day1;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class NewWindows {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
		driver.get("https://www.irctc.co.in/nget/train-search");
		
		driver.findElementByLinkText("AGENT LOGIN").click();
//switch  control from one window to next window
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allWindows = driver.getWindowHandles();
		List<String> ls= new ArrayList<String>();
		ls.addAll(allWindows);
		driver.switchTo().window(ls.get(1));
		System.out.println(driver.getTitle());
		
//Switch control to fwindow again
		//allWindows=driver.getWindowHandles();
		//ls=new ArrayList<String>();
		ls.addAll(allWindows);
		driver.switchTo().window(ls.get(0));
//closing the window
		driver.close();


	}

}
