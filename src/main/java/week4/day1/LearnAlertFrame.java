package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlertFrame {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		WebElement myFrame = driver.findElementById("iframeResult");
		driver.switchTo().frame(myFrame);
		driver.findElementByXPath("//button[text()=\"Try it\"]").click();
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();
		System.out.println("AlertText is: "+ "("+alertText +")");
//		driver.switchTo().alert().sendKeys("Kavitha");
		alert.sendKeys("kavitha");
		alert.accept();
		//driver.switchTo().frame(myFrame);
		//driver.switchTo().frame("iframeResult");
		String text = driver.findElementById("demo").getText();
		if(text.contains("Harry")) {
			System.out.println("The message verified");
		}
		
	
		driver.switchTo().defaultContent();
	
	
	}

}
