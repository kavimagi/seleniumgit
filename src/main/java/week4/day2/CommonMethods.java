package week4.day2;

import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonMethods {

	public static void main(String[] args) {
		public void methods() {
			System.setProperty("WebDriver.Chrome.driver", "./driver/chromedriver.exe");
			ChromeDriver driver=new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("url");
			WebElement options = driver.findElementById("");
			Select sel = new Select(options);
			sel.selectByVisibleText("");
			sel.selectByValue("");
			sel.selectByIndex(6);
			
			//get the count of options tag
			List<WebElement> allOpt = sel.getOptions();
			allOpt.size();
			
			//read text
			
			WebElement val = driver.findElementById("");
			val.getText();
			
			//current title
			
			WebElement url = driver.findElementById("");
			driver.getTitle();
			driver.getCurrentUrl();
			
		WebElement check = driver.findElementByName("");
		check.isSelected();
		
		//get title from second window
		
		Set<String> fwin = driver.getWindowHandles();
		ArrayList<String> ls = new ArrayList<String>();
		
		//adding value from set to list is 2 methods
		//one is ls.addAll(fwin); and next is ArrayList<String> ls = new ArrayList<String>(fwin);
		ls.addAll(fwin);
		driver.switchTo().window(ls.get(2));
		driver.getTitle();
		
		//take ss
		driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./d/img.png");
		FileUtils.copyFile(src,des);
		
		//Alert
		
		Alert alert = driver.switchTo().alert();
		alert.accept();
		alert.dismiss();
		alert.sendKeys("");
		
		//Frames
		
		WebDriver frame = driver.switchTo().frame(1);
		//driver.switchTo().frame(nameOrId);
		//WebElement frameEle = driver.findElementById("");
		//frameEle.switchTo().frame();

		driver.switchTo().defaultContent();
		//this is related to nested frame-if u want to switch from child 
// to parent frame use this method-driver.switchTo().parentFrame();
		
		
		
	//4th row-3rd column
	WebElement table = driver.findElementById("");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		List<WebElement> column =rows.get(3).findElements(By.tagName("td"));
		System.out.println(column.get(2).getText());
				
		//wait
		Thread.sleep(2000);
	
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		WebElement element = driver.findElementById("");
		WebDriverWait wait=new WebDriverWait(driver,10);
	
		wait.until(ExpectedConditions.elementToBeClickable(element));
		
		//
		
		
		
		
		
		
		
		
		
		
		
		
		
			
			
			
			
			
			
		}
	}

}
