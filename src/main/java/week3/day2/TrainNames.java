package week3.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TrainNames {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
		driver.get("http://erail.in");
		
	Thread.sleep(3000);
		driver.findElementByXPath("//input[@id='txtStationFrom']").clear();
		driver.findElementByXPath("//input[@id='txtStationFrom']").sendKeys("MAS",Keys.TAB);
		driver.findElementByXPath("//input[@id='txtStationTo']").clear();
		driver.findElementByXPath("//input[@id='txtStationTo']").sendKeys("SBC",Keys.TAB);
		boolean selected = driver.findElementById("chkSelectDateOnly").isSelected();
		if(selected) {
			driver.findElementById("chkSelectDateOnly").click();
		}
		//finding table
		
	WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
	
	//find row
	List<WebElement> rows = table.findElements(By.tagName("tr"));
	System.out.println(rows.size());
	
	for(int i=0;i<rows.size();i++) {
		WebElement eachRow = rows.get(i);
		List<WebElement> column = eachRow.findElements(By.tagName("td"));
		String trainName = column.get(1).getText();
		System.out.println(trainName);
		
		//Collections.reverse(trainName);
		
	}
	
	//Collections.reverse(trainName);
	
	}

}
