package week3.day1.selenium;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginLeafTabs {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
ChromeDriver driver =new ChromeDriver();
driver.manage().window().maximize();
driver.get("http://leafTaps.com/opentaps/");
driver.findElementById("username").sendKeys("DemoSalesManager");
driver.findElementById("password").sendKeys("crmsfa");
driver.findElementByClassName("decorativeSubmit").click();
driver.findElementByPartialLinkText("CRM/SFA").click();
driver.findElementByLinkText("Create Lead").click();
driver.findElementById("createLeadForm_companyName").sendKeys("createCompany");
driver.findElementById("createLeadForm_firstName").sendKeys("Kavitha");
driver.findElementById("createLeadForm_lastName").sendKeys("Ravi");
//dropdown1
WebElement eleSource = driver.findElementById("createLeadForm_dataSourceId");
Select sel=new Select(eleSource);
sel.selectByVisibleText("Employee");
//dropdown
WebElement optMarket = driver.findElementById("createLeadForm_marketingCampaignId");
Select sel1=new Select(optMarket);
sel1.selectByValue("CATRQ_CARNDRIVER");
//dropdown
WebElement optInd = driver.findElementById("createLeadForm_industryEnumId");
Select sel2=new Select(optInd);
List<WebElement> allOptions = sel2.getOptions();
System.out.println(allOptions.size());
sel2.selectByIndex(allOptions.size()-1);
for (WebElement eachValue : allOptions) {
	String txt = eachValue.getText();
	if(txt.startsWith("M"))
	System.out.println(txt);
	
}

driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Revathi");
driver.findElementById("createLeadForm_personalTitle").sendKeys("nothing");
driver.findElementById("createLeadForm_generalProfTitle").sendKeys("title");
driver.findElementById("createLeadForm_annualRevenue").sendKeys("1000");
//dropdown
WebElement optOwner = driver.findElementById("createLeadForm_ownershipEnumId");
Select sel3 = new Select(optOwner);
List<WebElement> options = sel3.getOptions();
options.size();
sel3.selectByIndex(options.size()-4);
driver.findElementById("createLeadForm_sicCode").sendKeys("100");
driver.findElementById("createLeadForm_description").sendKeys("Description");
driver.findElementById("createLeadForm_importantNote").sendKeys("Notes");
driver.findElementById("createLeadForm_lastNameLocal").sendKeys("suresh");
driver.findElementById("createLeadForm_departmentName").sendKeys("company");
driver.findElementById("createLeadForm_numberEmployees").sendKeys("150");
driver.findElementById("createLeadForm_tickerSymbol").sendKeys("1a");
//calender
//driver.findElementById("createLeadForm_birthDate-button").click();
//contact information
driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("104");
driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("1478");
driver.findElementById("createLeadForm_primaryEmail").sendKeys("kavimagi@gmail.com");
driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("1234567890");
driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Kavi");
driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("https://google.com");
//primary address
driver.findElementById("createLeadForm_generalToName").sendKeys("nname");
driver.findElementById("createLeadForm_generalAddress1").sendKeys("address");
driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600053");
WebElement state = driver.findElementById("createLeadForm_generalStateProvinceGeoId");

Select sel4 = new Select(state);
sel4.selectByIndex(15);
//create Lead
driver.findElementByName("submitButton").click();
//driver.close();
driver.findElementByXPath("//a[contains(text(),'Find Leads')]").click();
//entering firstname
driver.findElementByXPath("(//label[text()='First name:'])[3]/following::div[1]/input").sendKeys("Kavitha");
driver.findElementByXPath("//button[contains(text(),'Find Leads')]").click();
//clicking on first result
driver.findElementByXPath("(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first '])[1]").click();
Thread.sleep(3000);

//taking ss
//File src = driver.getScreenshotAs(OutputType.FILE);
//File des = new File("./snaps/img.png");
	//FileUtils.copyFile(src, des);

//Merge Leads
	driver.findElementByLinkText("Merge Leads").click();
	
	driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1").click();
	//second window
	Set<String> allwin = driver.getWindowHandles();
	List<String> ls=new ArrayList<String>();
	ls.addAll(allwin);
	driver.switchTo().window(ls.get(1));
	Thread.sleep(2000);
	//enter name in second window
	driver.findElementByXPath("//input[@name='firstName']").sendKeys("Kavitha");
	driver.findElementByXPath("//button[text()='Find Leads']").click();
	driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
	
	//switch 1st window
	driver.switchTo().window(ls.get(0));
	System.out.println("tiltel of 1st window"+driver.getTitle());
	
	
	
	}
}
