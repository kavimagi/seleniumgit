
import java.util.Scanner;

public class OddNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int i=0,number,sum=0;
		
		System.out.println("Enter the number to find the sum of odd numbers");
		Scanner input=new Scanner(System.in);
		number=input.nextInt();
		int wholenumber = number;
		
		while(number>0)
		{
			i = number%10;
			if(i%2!=0)
				sum=sum+i;
			number=number/10;
			
		}
		System.out.println("Sum of Odd numbers in the given number :"+sum);

	}

}
